<?php 
/**
 * @file
 * The Unity 3D logo image.
 *
 * Replaces the web player (e.g. for use in teasers).
 */
// For each field entry, display the logo image.
foreach ($items as $item) :
  echo $item['image'];
endforeach; ?>
