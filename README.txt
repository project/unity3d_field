About this module
------
Module that allows Unity3D Web Player files to be viewed when uploaded
as an attached field.

Author
------
Royce Kimmons
royce@kimmonsdesign.com

Installation
------------
Copy the Unity3D Field module directory to your modules directory and then 
enable on the admin modules page.  Create a content type (or edit an 
existing content type), and add a file field to it.  Set the file field 
display setting to "Unity 3D Player" or "Unity 3D Logo".
